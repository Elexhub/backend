# ![x](assets/e-bd.svg)

> Elexhub needs a backend, and MEN is the the solution.

```yml
MEN:
    M:
        MongoDB
    E:
        Express
    N:
        NodeJS
```
