const Category = require("../models/category.js");
const slugify = require("slugify");

function createCategories(cates, parentId = null) {
  const categoryList = [];
  let cate;

  if (parentId == null) {
    cate = cates.filter((cat) => cat.parentId == undefined);
  } else {
    cate = cates.filter((cat) => cat.parentId == parentId);
  }

  for (let cat of cate) {
    categoryList.push({
      _id: cat._id,
      name: cat.name,
      slug: cat.slug,
      children: createCategories(cates, cat._id),
    });
  }

  return categoryList;
}

exports.addCategory = (req, res) => {
  const categoryObj = {
    name: req.body.name,
    slug: slugify(req.body.name),
  };

  if (req.body.parentId) {
    categoryObj.parentId = req.body.parentId;
  }

  const cat = new Category(categoryObj);
  cat.save((err, category) => {
    if (err) return res.status(400).json({ err });
    if (category) {
      return res.status(201).json({ category });
    }
  });
};

exports.getCategories = (req, res) => {
  Category.find({}).exec((err, categories) => {
    if (err) return res.status(400).json({ err });

    if (categories) {
      const categoryList = createCategories(categories);

      res.status(200).json({ categoryList });
    }
  });
};
