const express = require("express");
const app = express();
const env = require("dotenv");
const mongoose = require("mongoose");
const { DC } = require("./chat/msgs.ts");

env.config();
const PORT = process.env.PORT;

// routes
const authRoutes = require("./routes/auth");
const adminRoutes = require("./routes/admin/auth");
const cateRoutes = require("./routes/category");
const productRoutes = require("./routes/product");

// mongodb connection
mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASS}@${process.env.CLUSTER_NAME}.bbqcn.mongodb.net/${process.env.MONGO_DB_DATABASE}?retryWrites=true&w=majority`,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }
  )
  .then(() => {
    console.log(DC);
  });

app.use(express.json());
app.use("/api", authRoutes);
app.use("/api", adminRoutes);
app.use("/api", cateRoutes);
app.use("/api", productRoutes);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT} ...`);
});
